Monitask是一个用于辅助基于遥感影像交互高效提取和编辑地理信息的QGIS插件。该插件利用当前较为成熟的深度学习AI模型，通过交互的方式，依据用户点击的提示信息，实时对影像按用户意图进行分割形成图斑，并提供了相应的灵活标注功能，方便用户对图斑进行类型标注和属性填充。还可利用QGIS内置的图形编辑功能，对生成的结果进行进一步的精细编辑以符合数据规范要求。

利用该插件，可以进行地理信息数据采集生产，特别对采集图斑类数据，可以大幅度提高工作效率。也可以利用该插件，开展遥感影像分类标注工作，可以大大提升AI模型训练样本数据采集的工作效率和样本质量。

此外，该插件对分割形成的图斑可以自动进行邻接图斑公共边协调，同类图斑合并，边线简化、平直化、光滑等后处理，大图斑自动扩展等特色功能，分割功能可以自动适应影像显示比例尺的变化，以便于降比例尺采集大面积图斑，也可升比例尺精细采集实地面积小的图斑。利用该插件，可以自定义适应自身需求的标注系统或分类系统，可自动辅助判断分割结果的类型，还支持对标注系统或分类系统根据标注结果进行优化调整。

安装方法，请参见[WIKI页面](https://gitee.com/grainseed/monitask/wikis)，如果嫌麻烦或有困难，也可以从[百度网盘](https://pan.baidu.com/s/177Y0KY0rqxATunR1CcFRmw?pwd=yiqc)直接下载已经配好运行环境的移动版QGIS-V3.30压缩包，下载后解压到硬盘根目录下，参照下图演示过程，点击解压目录下的QGIS_Monitask快捷图标即可运行使用，详细用法请参见[WIKI页面](https://gitee.com/grainseed/monitask/wikis)。

![User interface](resources/20231019_172141.gif)
As a QGIS plugin, Monitask is a SAM (facebook segment anything modal and its decendants) based geographic information extraction tool just by interactive click on remote sensing image, as well as an efficient geospatial labeling tool. While extracting geometries for geographic entities much more quickly, you can construct and optimize a land-cover classification system according to the nature characteristics of earth photo.

If you want to try it without difficult installation, please follow these links to download the compressed package (QGIS3.30.2-simplified_Monitask0.91.7z) which contains a portable QGIS3.30 with Monitask fully equipped. And unzip the package into any disk root, and click the QGIS_Monitask icon to run.
1. Google Drive : https://drive.google.com/file/d/1o52WTYhLzT5zuSH8WLd2EusYyFFBvlyF/view?usp=sharing
2. Baidu Drive: https://pan.baidu.com/s/177Y0KY0rqxATunR1CcFRmw?pwd=yiqc
For usage and more details, please goto https://gitee.com/grainseed/monitask/wikis

